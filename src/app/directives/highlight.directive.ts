import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';


@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  requiredStyles: {[key: string]: any} = {
    'background-color': 'yellow',
    'color': 'red',
    'font-weight': 'bold'
   };

  constructor(private el: ElementRef,
    private renderer: Renderer2) {
      // Object.keys(this.requiredStyles).forEach(style => {
      //   renderer.setStyle(el.nativeElement, `${style}`, this.requiredStyles[style]);
      // });
     }
  
  // @HostBinding('class')
  // elementClass = 'background-primary highlight';

  @HostListener('mouseenter') onmouseenter1() {
    this.renderer.addClass(this.el.nativeElement, 'highlight');
  }

  @HostListener('mouseleave') onmouseleave1() {
    this.renderer.removeClass(this.el.nativeElement, 'highlight');
  }

}
