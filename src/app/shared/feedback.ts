export class FeedBack {
    firstname: string = '';
    lastname: string = '';
    telnum: number = 0;
    email: string = '';
    agree: boolean = false;
    contacttype: string = '';
    message: string = '';
}

export const ContactType = ['None', 'Tel', 'Email'];