import { Component, OnInit, ViewChild } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { flyInOut } from '../animations/app.animation';
import { FeedBack, ContactType } from '../shared/feedback';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})
export class ContactComponent implements OnInit {

  feedbackform: FormGroup;
  feedback: FeedBack;
  contactType = ContactType;
  @ViewChild('fform') feedbackFormDirective: any;

  formErrors: {[key: string]: any} = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validatoinMessages: {[key: string]: any} = {
    'firstname': {
      'required': 'First name is required',
      'minlength': 'First name must be at least 2 characters long',
      'maxlength': 'First name cannot be more than 25 characters long'
    },
    'lastname': {
      'required': 'last name is required',
      'minlength': 'last name must be at least 2 characters long',
      'maxlength': 'last name cannot be more than 25 characters long'
    },
    'telnum': {
      'required': 'Tel. number name is required',
      'pattern': 'Tel. number must contain only numbers'
    },
    'email': {
      'required': 'email is required',
      'email': 'email not in valid format'
    }
  };

  constructor(private fb: FormBuilder) {
    this.createForm();
   }

   createForm() {
     this.feedbackform = this.fb.group({
       firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
       lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
       telnum: [0, [Validators.required, Validators.pattern]],
       email: ['', [Validators.required, Validators.email]],
       agree: false,
       contacttype: 'None',
       message: ''
     });

     this.feedbackform.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); //reset form validation messages
   }

  ngOnInit(): void {
  }

  onValueChanged(data?: any) {
    if (!this.feedbackform) { return; }
    const form = this.feedbackform;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error messages (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const message = this.validatoinMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += message[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.feedback = this.feedbackform.value;
    console.log(this.feedback);
    this.feedbackform.reset({
      firstname: '',
      lastname: '',
      telnum: 0,
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });

    // insure that feedback form is reset to its prestine value
    this.feedbackFormDirective.resetForm();
  }

}
