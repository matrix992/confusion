import { Component, Input, OnInit, ViewChild, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES} from '../shared/dishes';
import { Comment } from '../shared/comment';
import { DishService } from '../services/dish.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { expand, flyInOut, visibility } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  // @Input()
  dish: Dish | any;
  errMess: string;
  dishIds: string[];
  prev: string;
  next: string;
  dishCopy: Dish | any;
  visibility = 'shown';

  @ViewChild('formcomment') formcommentDirective: any;

  formErrors: {[key: string]: any} = {
    'name': '',
    'comment': ''
  };

  validationMessages: {[key: string]: any} = {
    'name': {
      'minlength': "Author name must be at least 2 characters",
      'required': "Name is required"
    },
    'comment': {
      'required': "Comment is required"
    }
  };

  formComment: FormGroup;

  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') public BaseURL: string) { 
      this.createFormComment();
    }

  createFormComment() {
    this.formComment = this.fb.group({
      name: ['', [Validators.required, Validators.minLength, Validators.maxLength]],
      rate: 5,
      comment: ['', Validators.required]
    });

    this.formComment.valueChanges.subscribe(
      data => this.onChangeData(data)
    );

    this.onChangeData();
  }

  onSubmit() {
    console.log("data", this.formComment.value);

    const comment: Comment = {
      author: this.formComment.get('name')?.value,
      comment: this.formComment.get('comment')?.value,
      rating: this.formComment.get('rate')?.value,
      date: new Date().toISOString()
    };
    
    this.dishCopy.comments.push(comment);
    this.dishservice.putDish(this.dishCopy)
      .subscribe(dish => {
        this.dish = dish;
        this.dishCopy = dish;
      },
      errmess => { this.dish = null; this.dishCopy = null; this.errMess = <any>errmess});

    this.reset();
  }

  reset() {
    this.formComment.reset({
      name: '',
      rate: 5,
      comment: ''
    });

    this.formcommentDirective.resetForm();
  }

  onChangeData(data?: any){
    if(! this.formComment) { return; }
    for(const controlName in this.formErrors){
      this.formErrors[controlName] = '';
      const control = this.formComment.get(controlName);
      if(control && control.dirty && !control.valid) {
        const messages = this.validationMessages[controlName];
        console.log('oh shit0');
        for(const key in control.errors) {
          console.log('oh shit1');
          this.formErrors[controlName] += messages[key] + ' ';
        }
      }
    }
  }

  formatSlider(value: number) {
    return value;
  }
  
  ngOnInit() {
    this.dishservice.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds);
    // const id = this.route.snapshot.params['id'];
    this.route.params.
      pipe(switchMap((params: Params) => { 
        this.visibility = 'hidden';
        return this.dishservice.getDish(params['id']); } ))
      .subscribe(dish => { this.dish = dish; this.dishCopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = <any>errmess);
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);

    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
    
  } 

  goBack(): void {
    this.location.back();
  }

}
